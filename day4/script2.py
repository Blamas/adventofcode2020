import argparse
import os
import sys

############################################################


def main():
    # defining a command line parser
    parser = argparse.ArgumentParser(description='InputFile')
    parser.add_argument('-f', '--file', type=str,
                        help='Input File', required=True)
    args = parser.parse_args()
    inFile = args.file

    # verify the existance of input file
    if not os.path.exists(inFile):
        sys.exit('The input file does not exist!')

    countValid = 0
    with open(inFile) as file:
        passportDict = {}
        for line in file:
            line = line.rstrip()
            if(line == ""):
                if all(key in passportDict.keys() for key in ["byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"]) and \
                        passportDict["byr"].isdigit() and 1920 <= int(passportDict["byr"]) <= 2002 and \
                        passportDict["iyr"].isdigit() and 2010 <= int(passportDict["iyr"]) <= 2020 and \
                        passportDict["eyr"].isdigit() and 2020 <= int(passportDict["eyr"]) <= 2030 and \
                        ((passportDict["hgt"][-2:] == "cm" and passportDict["hgt"][:-2].isdigit() and 150 <= int(passportDict["hgt"][:-2]) <= 193) or
                         (passportDict["hgt"][-2:] == "in" and passportDict["hgt"][:-2].isdigit() and 59 <= int(passportDict["hgt"][:-2]) <= 76)) and \
                        passportDict["hcl"][0] == "#" and len(passportDict["hcl"][1:]) == 6 and all(c in "0123456789abcdef" for c in passportDict["hcl"][1:]) and \
                        passportDict["ecl"] in ["amb", "blu", "brn", "gry", "grn", "hzl", "oth"] and \
                        len(passportDict["pid"]) == 9 and passportDict["pid"].isdigit():
                    countValid += 1
                passportDict = {}
            else:
                passportDict.update(item.split(":")
                                    for item in line.split(" "))

    print(countValid)


if __name__ == '__main__':

    main()
