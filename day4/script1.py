import argparse
import os
import sys
import re

############################################################


def main():
    # defining a command line parser
    parser = argparse.ArgumentParser(description='InputFile')
    parser.add_argument('-f', '--file', type=str,
                        help='Input File', required=True)
    args = parser.parse_args()
    inFile = args.file

    # verify the existance of input file
    if not os.path.exists(inFile):
        sys.exit('The input file does not exist!')

    requiredFiedls = ["byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"]
    countValid = 0
    with open(inFile) as file:
        testedFields = requiredFiedls.copy()
        for line in file:
            line = line.rstrip()
            if(line == ""):
                if len(testedFields) == 0:
                    countValid += 1
                testedFields = requiredFiedls.copy()
            else:
                for item in requiredFiedls:
                    if item in line:
                        testedFields.remove(item)

    print(countValid)


if __name__ == '__main__':

    main()
