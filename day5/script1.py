import argparse
import os, sys

############################################################
def main():
    #defining a command line parser
    parser = argparse.ArgumentParser(description='InputFile')
    parser.add_argument('-f','--file', type=str, help='Input File', required=True)
    args = parser.parse_args()
    inFile = args.file

    #verify the existance of input file 
    if not os.path.exists(inFile):
        sys.exit('The input file does not exist!') 

    seatMaxID = 0
    with open(inFile) as file:
        for line in file:
            line = line.replace("F", "0").replace("B", "1").replace("L", "0").replace("R", "1")
            seatMaxID = max(int(line,2), seatMaxID)
    
    print(seatMaxID)



if __name__ == '__main__':
           
    main()
