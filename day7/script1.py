import argparse
import os, sys
import re
import collections

############################################################

def main():
    #defining a command line parser
    parser = argparse.ArgumentParser(description='InputFile')
    parser.add_argument('-f','--file', type=str, help='Input File', required=True)
    args = parser.parse_args()
    inFile = args.file

    #verify the existance of input file 
    if not os.path.exists(inFile):
        sys.exit('The input file does not exist!') 

    dictColor = collections.defaultdict(list)
    with open(inFile) as file:
       for line in file:
            if "no" not in line:
                line = list(filter(None, line.strip().replace("contain", "").replace(",","").replace(".","").replace("bags", "").replace("bag", "").split(" ")))
                for i in range(2,len(line),3):
                    dictColor[line[0] + line[1]].append((int(line[i]),(line[i+1] + line[i+2])))
        
    def containsColor(color, testColor):
        isColor = False
        for (unused,subColor) in dictColor[color]:
            if subColor == testColor:
                isColor = True
            elif subColor in dictColor :
                isColor = isColor or containsColor(subColor)
        return isColor

    countColor = 0
    for color in dictColor.keys() :
        if containsColor(color,"shinygold"):
            countColor +=1
        
    print (countColor)

    def containsCount(color):
        totalCount = 0
        for (count,subColor) in dictColor[color]:
            if subColor in dictColor :
                totalCount += count + count * containsCount(subColor)
            else :
                totalCount += count
        return totalCount

    print(containsCount("shinygold"))

    

if __name__ == '__main__':
           
    main()
