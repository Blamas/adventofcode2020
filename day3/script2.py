import argparse
import os, sys
import re

############################################################
def main():
    #defining a command line parser
    parser = argparse.ArgumentParser(description='InputFile')
    parser.add_argument('-f','--file', type=str, help='Input File', required=True)
    args = parser.parse_args()
    inFile = args.file

    #verify the existance of input file 
    if not os.path.exists(inFile):
        sys.exit('The input file does not exist!') 

    valuesToTest = [(1,1), (3,1), (5,1), (7,1), (1,2)]
    total = 1
    for hstep, vstep in valuesToTest:
        countTrees=0
        with open(inFile) as file:
            counterOffsetHoriz = 0
            counterOffsetVert = 0
            for line in file:
                line = line.rstrip()
                if(counterOffsetVert % vstep == 0):
                    if(line[counterOffsetHoriz % len(line)] == "#"):
                        countTrees += 1
                    counterOffsetHoriz += hstep
                counterOffsetVert += 1
        total = total * countTrees
            
    print(total)


if __name__ == '__main__':
           
    main()
