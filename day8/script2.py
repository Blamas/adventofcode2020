import argparse
import os, sys

def swapIt(instructionList, instructionDone, index, accValue):
    instruction, value = instructionList[index]
    if instruction == "jmp":
            index += 1
    elif instruction == "nop":
        index += int(value)
    while index not in instructionDone:
        if index >= len(instructionList):
            return accValue
        instructionDone.append(index)
        instruction, value = instructionList[index]
        if instruction == "jmp":
            index += int(value)
        elif instruction == "acc":
            accValue += int(value)
            index += 1
        else:
            index += 1
    return -1

############################################################
def main():
    #defining a command line parser
    parser = argparse.ArgumentParser(description='InputFile')
    parser.add_argument('-f','--file', type=str, help='Input File', required=True)
    args = parser.parse_args()
    inFile = args.file

    #verify the existance of input file 
    if not os.path.exists(inFile):
        sys.exit('The input file does not exist!') 

    instructionList = []
    with open(inFile) as file:
        for line in file:
            instructionList.append(line.strip().split(" "))
    
    instructionDone = []
    index = 0
    accValue = 0
    while index < len(instructionList):
        instruction, value = instructionList[index]
        if instruction != "acc":
            result = swapIt(instructionList, instructionDone.copy(), index, accValue)
            if result > 0:
                accValue = result
                break
            else:
                if instruction == "jmp":
                    index += int(value)
                else :
                    index += 1
        else:
            accValue += int(value)
            index += 1

    print(accValue)

if __name__ == '__main__':
           
    main()
