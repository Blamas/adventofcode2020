import argparse
import os, sys
import re

############################################################
def main():
    #defining a command line parser
    parser = argparse.ArgumentParser(description='InputFile')
    parser.add_argument('-f','--file', type=str, help='Input File', required=True)
    args = parser.parse_args()
    inFile = args.file

    #verify the existance of input file 
    if not os.path.exists(inFile):
        sys.exit('The input file does not exist!') 

    validPassword = 0
    with open(inFile) as file:
        for line in file:
            splitLine = re.split(': |-| |\n', line)
            if int(splitLine[0]) <= splitLine[3].count(splitLine[2]) <= int(splitLine[1]):
                validPassword += 1
    print(validPassword)


if __name__ == '__main__':
           
    main()
