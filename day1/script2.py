import argparse
import os, sys

############################################################
def main():
    #defining a command line parser
    parser = argparse.ArgumentParser(description='InputFile')
    parser.add_argument('-f','--file', type=str, help='Input File', required=True)
    args = parser.parse_args()
    inFile = args.file

    #verify the existance of input file 
    if not os.path.exists(inFile):
        sys.exit('The input file does not exist!') 

    searchedNumber = 2020
    inputNumbers = []
    with open(inFile) as file:
        for line in file:
            if(int(line) < searchedNumber):
                inputNumbers.append(int(line))
    
    for index, firstValue in enumerate(inputNumbers,1):
        for index2, secondValue in enumerate(inputNumbers[index::],index+1):
            for thirdValue in inputNumbers[index2::]:
                if (firstValue + secondValue + thirdValue) == searchedNumber:
                    print(firstValue,secondValue,thirdValue)
                    print(firstValue*secondValue*thirdValue)
                    return

    return


if __name__ == '__main__':
           
    main()
